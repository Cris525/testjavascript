$(document).ready(function () {
    $("#search").keyup(function () {
        var valor = this
        $.each($("#miTabla tbody tr"), function () {
            if ($(this).text().toLowerCase().indexOf($(valor).val().toLowerCase()) === -1) {
                $(this).hide();
                $('.no-result').show();
            } else
                $(this).show();
        });
    });
    cargarTabla(0);
});

function ValidEmail(email) {
    var x = document.querySelector("#"+email);
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    return emailRegex.test(x.value);
}

function validarForm(nombre, texto) {
    var x = document.querySelector("#"+nombre);
    if (x.value == "") {
        $('#error' + nombre).css("display", "block");
        $('#error' + nombre).html('El campo ' + texto + ' es requerido');
    } else {
        if (nombre == 'telefono') {
            var newString = new libphonenumber.AsYouType('EC').input(x.value);
            $(this).focus().val('').val(newString);
        }
        if (nombre == 'email') {
            if (!ValidEmail(nombre)) {
                $('#error' + nombre).css("display", "block");
                $('#error' + nombre).html('El email es incorrecto');
            } else {
                $('#error' + nombre).css("display", "none");
//                x.value = x.value.toUpperCase();
            }
        } else {
            $('#error' + nombre).css("display", "none");
            x.value = x.value.toUpperCase();
        }
    }
}


function cargarTabla(id) {
    fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(json => generarTabla(json, id)
            );
}

function generarTabla(data, id) {
    var contenido = document.querySelector("#contenido");
    contenido.innerHTML = "";
    for (let datos of data) {
        if (datos.id != id) {
            contenido.innerHTML += '<tr><th scope="row">' + datos.id + '</th><td>' + datos.name + '</td><td>' + datos.username + '</td>'
                    + '<td>' + datos.email + '</td><td>' + datos.phone + '</td>'
                    + '<td>'
                    + '<button onclick="obtenerDetalle(' + datos.id + ')"  data-toggle="modal" data-target="#verInfo" class="btn btn-info"><i class="fas fa-info">Ver</i></button>'
                    + ' <button data-toggle="modal" data-target="#crear" class="btn btn-warning"><i class="fas fa-edit">Crear</i></button>'
                    + ' <button onclick="eliminarDetalle(' + datos.id + ')" data-toggle="modal" data-target="#eliminar" class="btn btn-danger"><i class="far fa-trash-alt">Eliminar</i></button>'
                    + '</td>'
                    + '</tr>';
        }
    }
}

function obtenerDetalle(idUsuario) {
    fetch('https://jsonplaceholder.typicode.com/users/' + idUsuario)
            .then(response => response.json())
            .then(json => verInformacion(json)
            );

}
function verInformacion(data) {
    var informacion = document.querySelector("#informacion");
    informacion.innerHTML = '<tr><td><b>Nombre</b></td><td>' + data.name + '</td>'
            + '<tr><td><b>Usuario</b></td><td>' + data.username + '</td>'
            + '<tr><td><b>Email</b></td><td>' + data.email + '</td>'
            + '<tr><td><b>Dirección</b></td><td>' + data.address.street + ' ' + data.address.suite + ' ' + data.address.city + ' ' + data.address.zipcode + '</br> <b>Lat: </b>' + data.address.geo.lat + '</br> <b>Lon: </b>' + data.address.geo.lng + '</td>'
            + '<tr><td><b>Teléfono</b></td><td>' + data.phone + '</td>'
            + '<tr><td><b>Sitio Web</b></td><td>' + data.website + '</td>'
            + '<tr><td><b>Empresa</b></td><td>' + data.company.name + '</br> <b>Dirección: </b>' + data.company.catchPhrase + ' ' + data.company.bs + '</td>'
            + '</tr>';
}
function eliminarDetalle(idUsuario) {
    var informacion = document.querySelector("#resultado");
    informacion.innerHTML = 'Usuario eliminado';
    fetch('https://jsonplaceholder.typicode.com/posts/' + idUsuario, {
        method: 'DELETE'
    })
            .then(response => response.json())
            .then(json => cargarTabla(idUsuario));
}

function crearDetalle() {
    var nombre = document.querySelector("#nombre").value;
    var apellido = document.querySelector("#apellido").value;
    var email = document.querySelector("#email").value;
    var fecNac = document.querySelector("#fecNac").value;
    var pais = document.querySelector("#pais").value;
    var telefono = document.querySelector("#telefono").value;
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            nombre: nombre,
            apellido: apellido,
            email: email,
            fecNac: fecNac,
            pais: pais,
            telefono: telefono
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    })
            .then(response => response.json())
            .then(json => {
                $("#crear").modal("hide");
            })
}

function autocompletar() {
    const pais = document.querySelector("#pais");
    let indexFocus = -1;

    pais.addEventListener("input", function () {
        const tipoPais = this.value;
        if (!tipoPais)
            return false;

        cerrarLista();
        const divList = document.createElement("div");
        divList.setAttribute("id", this.id + "-lista-autocompletar");
        divList.setAttribute("class", "lista-autocompletar-items");
        this.parentNode.appendChild(divList);

        fetch('https://restcountries.eu/rest/v2/name/' + pais.value)
                .then(response => response.json())
                .then(json => {
                    const arr = json;
                    console.log("arr= ", arr);
                    if (arr.length == 0)
                        return false;
                    arr.forEach(item => {
                        if (item.name.substr(0, tipoPais.length).toLowerCase == tipoPais.toLowerCase) {
                            const elementoLista = document.createElement("div");
                            elementoLista.innerHTML = `<strong> ${item.name.substr(0, tipoPais.length)}</strong>${item.name.substr(tipoPais.length)}  `;
                            elementoLista.addEventListener('click', function () {
                                pais.value = this.innerText;
                                cerrarLista();
                                return false;
                            });
                            divList.appendChild(elementoLista);
                        }
                    });
                });
    });

    pais.addEventListener('keydown', function (e) {
        const divList = document.querySelector('#' + this.id + '-lista-autocompletar');
        let items;

        if (divList) {
            items = divList.querySelectorAll('div');

            switch (e.keyCode) {
                case 40: //abajo
                    indexFocus++;
                    if (indexFocus > items.length - 1)
                        indexFocus = items.length - 1;
                    break;
                case 38: //arriba
                    indexFocus--;
                    if (indexFocus < 0)
                        indexFocus = 0;
                    break;
                case 13: //enter
                    e.preventDefault();
                    items[indexFocus].click();
                    indexFocus = -1;
                    break;
                default:
                    break;
            }
            seleccionar(items, indexFocus);
            return false;
        }
    });
    document.addEventListener('click', function () {
        cerrarLista();
    });
}

function seleccionar(lista, indexFocus) {
    if (!lista || indexFocus == -1)
        return false;
    lista.forEach(x => {
        x.classList.remove('autocompletar-active')
    });
    lista[indexFocus].classList.add("autocompletar-active");
}

function cerrarLista() {
    const items = document.querySelectorAll(".lista-autocompletar-items");
    items.forEach(item => {
        item.parentNode.removeChild(item);
    });
    indexFocus = -1;
}
