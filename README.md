# testJavascript

Para ejecutar el ejemplo debe tener instalado un servidor local.

# Instalación en windows
1. Instalar AppServ: https://www.appserv.org/en/download/
2. Copiar el ejemplo en la siguiente ruta: C:/AppServ/www/
3. Abrir el ejemplo en cualquier navegador con la ruta siguiente: http://localhost/testjavascript/index.html


# Instalación en linux
1. Instalar Xampp: https://www.apachefriends.org/download.html
2. Copiar el ejemplo en la siguiente ruta: opt/lampp/htdocs/
3. Abrir el ejemplo en cualquier navegador con la ruta siguiente: http://localhost/testjavascript/index.html